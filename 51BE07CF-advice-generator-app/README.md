# Frontend Mentor - Advice generator app solution

[Advice generator app challenge on Frontend Mentor](https://www.frontendmentor.io/challenges/advice-generator-app-QdUG-13db)

## Table of contents

- [Overview](#overview)
  - [Links](#links)
  - [Screenshots](#screenshots)
- [Study Cases](#study-cases)
- [Author](#author)
- [Acknowledgments](#acknowledgments)

## Overview

**Estimated Time**: 6h & ~23min

### Links

- [Solution URL](https://gitlab.com/ndt-frontendmentor/QdUG-13db-advice-generator-app)
- [Live Site URL](https://ndt-frontendmentor.gitlab.io/QdUG-13db-advice-generator-app)

### Screenshots

![](./screenshot/desktop.png)
![](./screenshot/mobile.png) 

## Study Cases

- HTML5/CSS3/Js
- Mobile-first workflow

## Author

- Website - [Nguyễn Đăng Tú](https://ngdangtu.com)
- Frontend Mentor -
  [@ngdangtu-vn](https://www.frontendmentor.io/profile/ngdangtu-vn)\
  (Even though I didn't wish to choose this username! There is nothing I can do
  about this 😭)
- Mastodon - [@ngdangtu@tilde.zone](https://tilde.zone/@ngdangtu)

## Acknowledgments

This is where I give a hat tip to anyone helps me or any project that backs my work.

