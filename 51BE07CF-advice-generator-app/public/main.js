const advice_box = document.querySelector('#advice')

let is_fetching = false
document.querySelector('#roll').addEventListener('click', async e =>
{
   console.log(is_fetching, e.target)
   if (is_fetching) return void 0

   advice_box.setAttribute('aria-busy', true)
   is_fetching = true

   const { id, advice } = await get_advice()
   console.log(is_fetching, advice)
   advice_box.dataset.id = id
   advice_box.querySelector('q').innerText = advice

   is_fetching = false
   advice_box.setAttribute('aria-busy', false)
})

async function get_advice()
{
   const res = await fetch(`https://api.adviceslip.com/advice?t=${Date.now()}`)
   const data = await res.json()
   return data.slip
}