/** the largest prime number smaller than 2¹⁶ */
const adler_mod = 65521
/**
 * Why Adler32: https://stackoverflow.com/a/12551654
 * What's Adler32: https://en.wikipedia.org/wiki/Adler-32
 */
export function adler32(input: string | number): string
{
   const str = input.toString()
   let alpha = 1, beta = 0

   for (let i = 0; i < str.length; i++)
   {
      const char = str.charCodeAt(i)
      alpha = (alpha + char) % adler_mod
      beta = (beta + alpha) % adler_mod
   }

   const result = (beta << 16) | alpha
   return result.toString(16)
}

export function throwerr(msg: string, code = 1)
{
   if (code === 0) return void 0
   console.error(`%cError: %c${msg}`, 'color:red', 'color:initial')
   Deno.exit(code)
}

export function capitalize(str: string)
{
   const initial = str.charAt(0).toUpperCase()
   return initial + str.slice(1)
}

export interface GitOutput
{
   code: number
   output: string
}
/**
 * Require `--allow-run=git` permission
 */
export async function git(args: string[], exitnow = true): Promise<GitOutput>
{
   const { code, stdout, stderr } = await new Deno.Command('git', { args }).output()

   const decoder = new TextDecoder()
   if (code > 0)
   {
      const err = decoder.decode(stderr)
      exitnow && throwerr(err, code)
      return { code, output: err }
   }

   return { code, output: decoder.decode(stdout) }
}

export function must_answer(question: string, suggested_answer?: string): string
{
   let answer: string | null = null
   while (!answer)
   {
      answer = prompt(question, suggested_answer)
   }
   return answer
}