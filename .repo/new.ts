#!/usr/bin/env -S deno run --allow-read --allow-write --allow-run --allow-env --allow-net=github.com

import { parseArgs } from "https://deno.land/std@0.213.0/cli/parse_args.ts"
import { copy } from "https://deno.land/std@0.213.0/fs/mod.ts"
import { adler32, git, throwerr, must_answer } from './_util.ts'


import "https://deno.land/std@0.213.0/dotenv/load.ts"
const p_base = Deno.env.get('BASE')!
const p_tmpl = Deno.env.get('TMPL')!


/** Main */


// 0. checking requirement
check_requirement()
const { fm_slug, p_dest, p_readme_fm, p_readme } = get_input()

// 1. git clone the challenge repo
await clone_challenge()

// 2. overwrite stuffs with template dir
const { name: fm_name, url: fm_url } = await get_fm_info(p_readme_fm)
await overwrite_with_tmpl()

// 3. replace $var with real data
update_readme()

// 4. print out reminder
console.log(
   `Challenge %c${fm_name}%c created, don't forget to update README file 😉`,
   'color: green',
   'color: initial',
)

/** Utilities */
async function get_fm_info(filepath: string)
{
   const search_pattern = /\[(.+) challenge on Frontend Mentor\]\((https\:\/\/www\.frontendmentor\.io\/challenges\/.+)\)./i

   const readme = await Deno.readTextFile(filepath)
   const [ _is_match, name, url ] = readme.match(search_pattern) ?? []
   _is_match || throwerr('Unable to find ')

   return { name, url }
}

function check_requirement()
{
   if (!p_base) throwerr('Missing base path of this project. Make sure BASE key exists in `.env` file.')
   if (!p_tmpl) throwerr('Missing template challenge directory. Make sure TMPL key exists in `.env` file.')
}

interface FormatData
{
   fm_slug: string
   variant_name?: string
   p_dest: string
   p_readme_fm: string
   p_readme: string
}
function get_input(): FormatData
{
   const [ slug, variant ] = parseArgs(Deno.args)._

   if (!slug) console.log(
      'Require the challenge slug name! \nYou can find some good challenge in here: %chttps://www.frontendmentor.io/challenges',
      'color:blue'
   )
   const fm_slug = slug?.toString() ?? must_answer('Please enter challenge slug name you wish to start:', 'recipe-page')

   const p_dest = `${adler32(fm_slug).toUpperCase()}-${variant ?? fm_slug}`


   return {
      fm_slug,
      variant_name: variant?.toString(),
      p_dest,
      p_readme_fm: `${p_dest}/README-template.md`,
      p_readme: `${p_dest}/README.md`,
   }
}

async function clone_challenge()
{
   const { output } = await git([
      'clone',
      `https://github.com/frontendmentorio/${fm_slug}.git`,
      p_dest
   ])
   return output
}

async function overwrite_with_tmpl()
{
   await copy(p_tmpl, p_dest, { overwrite: true })
   Deno.rename(`${p_dest}/style-guide.md`, `${p_dest}/design/style-guide.md`)
   await Deno.remove(`${p_dest}/.git`, { recursive: true })
}

async function update_readme()
{
   const readme = (await Deno.readTextFile(p_readme))
      .replaceAll('$FMname', fm_name)
      .replaceAll('$FMurl', fm_url)
      .replaceAll('$solutionPath', `${p_base}/${p_dest}`)

   await Deno.writeTextFile(p_readme, readme)
   await Deno.remove(p_readme_fm)
}
