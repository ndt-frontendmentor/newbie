import { throwerr } from './_util.ts'


export interface BuildConfig
{
   build_challenge: (name: string, dist: string) => void | Promise<void>
   dist_path: string
   challenge_dir_name?: string
}

export async function build(cfg: BuildConfig)
{
   const { challenge_dir_name, build_challenge, dist_path } = cfg

   if (challenge_dir_name)
   {
      const { isDirectory } = await Deno.stat(name)
      if (!isDirectory) throwerr('The input name is not a directory.')
      await build_challenge(challenge_dir_name, dist_path)
      Deno.exit(0)
   }

   const ls = Deno.readDir('.')
   for await (const entry of ls)
   {
      if (!entry.isDirectory) continue
      if (entry.name.startsWith('.')) continue
      if (entry.name === dist_path) continue

      build_challenge(entry.name, dist_path)
   }
}
