# Frontend Mentor - Recipe page solution

[Recipe page challenge on Frontend Mentor](https://www.frontendmentor.io/challenges/recipe-page-KiTsR8QQKm)

## Table of contents

- [Overview](#overview)
  - [Links](#links)
  - [Screenshots](#screenshots)
- [Study Cases](#study-cases)
- [Author](#author)

## Overview

**Estimated Time**: 10h & ~29min

### Links

- [Solution URL](https://gitlab.com/ndt-frontendmentor/KiTsR8QQKm-recipe-page)
- [Live Site URL](https://ndt-frontendmentor.gitlab.io/KiTsR8QQKm-recipe-page)

### Screenshots

![Desktop screenshot](./screenshot/desktop.png)
![Mobile screenshot](./screenshot/mobile.png)

## Study Cases

- HTML5/CSS3
  - https://whatunit.com
- Mobile-first workflow
- Pure simple static site (no generator)

## Author

(see [About Me section in root README](../README.md#about-me))
