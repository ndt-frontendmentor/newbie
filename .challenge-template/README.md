# Frontend Mentor - $FMname

[$FMname challenge on Frontend Mentor]($FMurl)

## Table of Contents

- [Overview](#overview)
  - [Links](#links)
  - [Screenshots](#screenshots)
- [Case Studies](#case-studies)
- [Author](#author)
- [Acknowledgments](#acknowledgments)

## Overview

**Estimated Time**: ?d ?h ~?min

### Links

- [Solution URL](https://gitlab.com/ndt-frontendmentor/$solutionPath)
- [Live Site URL](https://ndt-frontendmentor.gitlab.io/$solutionPath)

### Screenshots

![](./screenshot/desktop.png) ![](./screenshot/mobile.png)

## Case Studies

- HTML5/CSS3/Js
- Mobile-first workflow

## Author

(see [About Me section in root README](../README.md#about-me))

## Acknowledgments

This is where I give a hat tip to anyone helps me or any project that backs my
work.
