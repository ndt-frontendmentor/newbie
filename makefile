.PHONY: new build dev pubtag

P=./.repo

# $p = Project Pathname (from the official website)
# $v = Project Variant (create same ID of orig prj but different name)
new:
	@${P}/new.ts $p $v

# $c = Challenge (specific to 1 challenge)
# No flag = build/rebuild everything
build:
	@${P}/build.ts $c

dev:
	@${P}/localhost.ts

# $t = Tag Name
# $d = Tag Description
# No flag = interactive CLI
pubtag:
	@${P}/pubtag.ts $t $d