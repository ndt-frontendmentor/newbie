# My Solutions for Newbie Challenges of Frontend Mentor

This is my solution repo for **newbie** challenges.

**What is Frontend Mentor** (FM)?

> Our professionally designed challenges help you gain hands-on experience
> writing HTML, CSS, and JavaScript. We create the designs so you can focus on
> the code and see your skills skyrocket!
>
> https://www.frontendmentor.io

## Requirement

This repo is designed to work in Linux environment. Probably it can work on
other OS implement UNIX standard as long as it has there tools:

1. [Deno](https://deno.land)
2. [Git](https://git-scm.com)

It doesn't get test on Windows and MacOS yet, so I would appreciated if you guys
could share me your experience.

## Instruction

### Start new challenge

The script will download source challenge and mixes with template directory. All
you need is to give it correct slug name. You can found it in any public
challenge URL.

```shell
make new p=challenge-slug
```

For example, I plan to do this challenge
`https://www.frontendmentor.io/challenges/recipe-page-KiTsR8QQKm`. Therefore,
the challenge-slug will be `recipe-page`.

If you ever want to do another solution for a submitted challenge (make a
variant solution), you can use `v` flag:

```shell
make new p=challenge-slug v=another-solution
```

### Build a challenge

To build a specific challenge, run

```shell
make build c=challenge-slug
```

To build all, which will **overwrite** all the old builts, run

```shell
make build
```

### Local testing/debug

The repo comes with a super simple server that can serve static files from
`public/` directory. Just run the command below:

```shell
make dev
```

### Deployement

This repo is made to do very simple deploy on Gitlab. If you want to use it on
other source code hosting like Github. You will need to modify a bit. As for
Gitlab users, all you have to do is submit a new tag, the CI/CD system will
automatic copy all the files in `public/` and publish it to their Gitlab Page.

Here is step by step guide for deploying on **Gitlab**:

1. Build all the challenge you want to publish
2. Create a tag in localhost, recommend annotation tag:
   `git tag -a v1.0 -m 'version 1.0'`
3. Double check on everything before it too late :D
4. Push tag: `git push -u origin v1.0`

**OR do this:**

Create & PUBLISH tag with this comment:

- `make pubtag` → later on enter tag name & desc → wait
- `make pubtag t=1.0 d="version 1.0"` → wait

## About Me

- Website - [Nguyễn Đăng Tú][ln-site]
- Frontend Mentor - [@ngdangtu-vn][ln-frontendmentor]\
  (Even though I didn't wish to choose this username! There is nothing I can do
  about this 😭)
- Mastodon - [@ngdangtu@tilde.zone][ln-mastodon]

[ln-site]: https://ngdangtu.com
[ln-frontendmentor]: https://www.frontendmentor.io/profile/ngdangtu-vn
[ln-mastodon]: https://tilde.zone/@ngdangtu

## Acknowledgments

Repository Icon credit to
[`hatching chick`](https://iconduck.com/emojis/97173/hatching-chick) with
[`CC-BY-4.0`](https://github.com/twitter/twemoji/blob/master/LICENSE-GRAPHICS).
The original icon has been slightly modified for my personal use purpose.

This repo is created base on the
[Template Repository for Frontend Mentor Challenges](https://gitlab.com/ndt-frontendmentor/temp-repo)
created by [Đăng Tú (ngdangtu)](https://ngdangtu.com)
