# Frontend Mentor - Social links profile solution

[Social links profile challenge on Frontend Mentor](https://www.frontendmentor.io/challenges/social-links-profile-UG32l9m6dQ)

## Table of contents

- [Overview](#overview)
  - [Links](#links)
  - [Screenshot](#screenshot)
- [Study Cases](#study-cases)
- [Author](#author)

## Overview

**Estimated Time**: 9h & ~12min

### Links

- [Solution URL](https://gitlab.com/ndt-frontendmentor/UG32l9m6dQ-social-links-profile)
- [Live Site URL](https://ndt-frontendmentor.gitlab.io/UG32l9m6dQ-social-links-profile)

### Screenshots

![Desktop screenshot](./screenshot/desktop.png)
![Mobile screenshot](./screenshot/mobile.png)

## Study Cases

- HTML5/CSS3
- Mobile-first workflow
- Pure simple static site (no generator)

## Author

- Website - [Nguyễn Đăng Tú](https://ngdangtu.com)
- Frontend Mentor -
  [@ngdangtu-vn](https://www.frontendmentor.io/profile/ngdangtu-vn)\
  (Even though I didn't wish to choose this username! There is nothing I can do
  about this 😭)
- Mastodon - [@ngdangtu@tilde.zone](https://tilde.zone/@ngdangtu)
